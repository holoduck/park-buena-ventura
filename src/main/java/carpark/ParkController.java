/**
* This module contains the web app controller.
* The code is structured by grouping together http messages GETs and POSTs of the same mapping.
*/

package carpark;

import java.util.LinkedList;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class ParkController{
    private ParkingFloor parking;

    public ParkController(){
        this.parking = new ParkingFloor();
    }
    
    /*** /  (alias to park)***/
    
    @RequestMapping(value="/", method=RequestMethod.GET)
    public String root(Model model) {
        model.addAttribute("vehicle", new Vehicle());
        return "parkcar";
    }

    /*** /park  ***/
    
    /**
    * Ask the client to define its vehicle size in order to require to park via POST.
    */
    @RequestMapping(value="/park", method=RequestMethod.GET)
    public String parkForm(Model model) {
        return "parkcar";
    }
    
    /**
    * Request to park a vehicle of size @size.
    * If successful, shows a "response".
    * If the parking is full, the method shows such a notification.
    * If the size of the vehicle is not accepted, an error is shown.
    @param size size of the vehicle, if the size does not match the system definition, an error is shown.
    */
    @RequestMapping(value="/park", method=RequestMethod.POST)
    public String parkHandler(Model model, @RequestParam(value="size", required=true) String size) {
        Spot chosenSpot;
        Vehicle v;

        switch (size){
            case "Moto":
                v = new Vehicle(Size.Moto);
                break;
            case "Car":
                v = new Vehicle(Size.Compact);
                break;
            case "Bus":
                v = new VBus();
                break;
            default:
                v = null;
                break;
        }
        
        if (v == null) {return "error";}
        
        chosenSpot = parking.park(v);
        
        if (chosenSpot != null){
            model.addAttribute("spot", chosenSpot);
            return "response";
        }
        
        return "full";
    }
    
    
    /***  /map  ***/
    
    /*
    * Shows the parking map.
    */
    @RequestMapping(value="/map", method=RequestMethod.GET)
    public String parkMap(Model model) {
        model.addAttribute("map", parking);
        return "map";
    }
    
    /*
    * Accept a vehicle number and, if parked, frees its spot.
    * This is equivalent to make the vehicle leave.
    */
    @RequestMapping(value="/map", method=RequestMethod.POST)
    public String leaveSubmit(Model model, @RequestParam(value="action", required=true) String action) {
        int code;
        
        try {
            code = Integer.parseInt(action);
        }
        catch (NumberFormatException nfe){
            System.out.println("HACK ALERT: Received a non-number vehicle label!");
            code = -1;
        }
        
        if (code > -1) {parking.leave(code);}
        
        model.addAttribute("map", parking);
        return "map";
    }
    
    
    /*** /leave ***/
    /*** Leave a vehicle w/ using the parking map*/
    
    @RequestMapping(value="/leave", method=RequestMethod.GET)
    public String leaveHandler(Model model) {
        model.addAttribute("leavingCar", new Code() );
        return "leave";
    }
    
    @RequestMapping(value="/leave", method=RequestMethod.POST)
    public String leaveSubmit(Model model, @ModelAttribute Code leavingCar) {
        System.out.println("Req to leave V: " + leavingCar.getCode());
        parking.leave(leavingCar.getCode());
        model.addAttribute("map", parking);
        return "map";
    }
    
}
