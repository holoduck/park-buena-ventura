# Park Buena Ventura (Spring Web Development Exercise)  #

### What the App does ###

Imagine to drive in front of a Car Parking.
At the entrance a screen asks the size of the car you want to park
and if possible it assigns to you a free parking slot.
A map of the parking is available through which you can select and free occupied slots.

See the app in action on [Heroku](https://hidden-spire-58130.herokuapp.com/park)!

### Tools:###

* Java 8
* Spring
* Gradle
* Heroku

This is a full repository ready to deploy on Heroku.

### License ###
GNU AFFERO GENERAL PUBLIC LICENSE
(See COPYING)