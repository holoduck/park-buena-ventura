package carpark;

/**
* Row of consecutive spots of the same size.
* This class is responsible to keep a geographical and logical grouping of the parking spots.
*/
public class Row{

    private static int rowNumber = 0;
    
    public Spot[] spots;
    protected int freeSpots;
    protected Size size;
    private int myNumber;
    
    public Row(int numberOfSpots, Size s){
        spots = new Spot[numberOfSpots];
        myNumber = rowNumber++;
        
        for (int i=0; i < numberOfSpots; i++){
            spots[i] = new Spot(new int[]{i,rowNumber}, String.valueOf(rowNumber) + String.valueOf(i));
        }
        
        size = s;
        freeSpots = numberOfSpots;
    }
    
    /**
    * Park a vehicle in this row.
    * Vehicles are not accepted if too big or there are no available free spots.
    @param v the vehicle to park.
    @return the Spot assigned to the vehicle, null if it was impossible to park in this row.
    */
    public Spot park(Vehicle v){
        if ((v.getSize().compareTo(size) > 0) || (freeSpots == 0)) {return null;}
        
        int slot = 0;
        while (slot < spots.length && !spots[slot].allocate(v)){
            slot++;
        }
        
        if (slot == spots.length) {
            return null;
        }
        
        freeSpots--;
        v.setRow(this);
        
        return spots[slot];
    }
    
    /**
    * Acknowledge a vechile has left.
    * This methd does free the vehicle spot(s).
    */
    public void freeSlot(){
        freeSpots++;
    }
    
    public String toString(){
        return "Row" + myNumber + " (" + size + ") ";
    }
    
    public String ofSize(){ return " (" + size + " size) ";}

}




