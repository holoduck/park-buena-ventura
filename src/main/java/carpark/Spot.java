package carpark;

/**
* Spot of the parking floor.
* A spot can be free or assigned to a vehicle of its size or smaller.
*/
public class Spot {
    private boolean free;
    private int[] coordinate;
    private String code;
    private Vehicle vehicle;
    
    protected Spot(int[] coordinate, String code){
        this.coordinate = coordinate;
        this.code = code;
        free = true;
    }
    
    /**
    * Assign a vehicle to this spot.
    */
    public boolean allocate(Vehicle v){
        if (free){
            v.assignSlot(this);
            vehicle = v;
            
            signalAllocated(v);
            
            free = false;
            return true;
        }
        
        return false;
    }
    
    /**
    * Free this spot by the occuping vehicle.
    * If the spot is already free, nothing happens.
    */
    public void deallocate(){
        free = true;
        showStatus();
    }
    
    /**
    * Test whether the spot is free.
    @return true if the spot is free, false otherwise.
    */
    public boolean isFree(){
        return free;
    }
    
    public String getCode(){
        return code;
    }
    
    private void showStatus(){
        String status;
        
        if (free) {
            status = "free";
        }
        else {
            status = "occupied";
        }
        
        System.out.println("Spot " + code + " at " + coordinate[0] + " " + coordinate[1] + " is " + status);
    }
    
    private void signalAllocated(Vehicle v){
        System.out.println("Spot " + code + " at " + coordinate[0] + " " + coordinate[1] + " was taken by " + v);
    }
    
    public String toString(){
        if (free) {
            return "Free";
        }
        else {
            return vehicle.toString();
        }
    }
    
    /**
    * The code of the vehicle on this spot.
    @return if the spot is free -1, otherwise the code of the vehicle.
    */
    public int getCarCode(){
        if (free) {return -1;}
        else {return vehicle.getCode();}
    }
    
}







