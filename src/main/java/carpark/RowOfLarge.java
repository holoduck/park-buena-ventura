package carpark;

/**
* This class implements special methods for handle Bus-size vehicles.
*/
public class RowOfLarge extends Row{
    
    public RowOfLarge(int numberOfSpots, Size s){
        super(numberOfSpots, s);
    }
    
    public Spot park(Vehicle v){
        if (v.getSize() == Size.Bus) {return parkBus((VBus)v);}
        else return super.park(v);
    }
    
    private Spot parkBus(VBus v){
        if (freeSpots < 5) {return null;}
        
        System.out.println("I will try to park this Bus!");
        System.out.println("I have " + freeSpots + " free spots");
        
        int startingSlot = findFive();
        
        System.out.println("I got " + startingSlot + " as startingSlot");
        
        if (startingSlot == -1) {return null;}
        
        for(int i = startingSlot; i < startingSlot + 5; i++){
            spots[i].allocate(v);
        }
        
        freeSpots--;
        v.setRow(this);
        
        System.out.println("I will give this slot:" + spots[startingSlot]);
        
        return spots[startingSlot];
    }
    
    /**
    * Search for a five adjancent free spots in the row.
    * @return the index of the first free spots in the row of five.
    */
    private int findFive(){
        int i = 0;
        int conseq = 0;
        
        while(i < (spots.length - 3)) {
            System.out.println("i: "+ i + " conseq: " + conseq);
            
            while(i < (spots.length) && spots[i].isFree()){
                conseq++;
                
                if (conseq == 5) {
                    System.out.println("returning i=" + i);
                    return i-4;
                }
                
                i++;
            }
            
            System.out.println("Setting conseq to 0");
            conseq = 0;
            i++;
            
        }
        
        return -1;
    }
    
    public void freeSlot(int numberOfSlots){
        freeSpots += 5;
    }
}
