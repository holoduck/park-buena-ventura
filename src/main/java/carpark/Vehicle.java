package carpark;

/**
* A vehicle.
*/
public class Vehicle {
    private Size size;
    private Spot slot;
    private Row row;
    private int code;
    
    /**
    @param s must be smaller than Bus.
    */
    public Vehicle(Size s) {
        this.size = s;
    }
    
    public Vehicle(){
    }
    
    /**
    * Notifies the assigned slot and leave.
    */
    public void leave(){
        slot.deallocate();
        slot = null;
        row.freeSlot();
    }
    
    public void assignSlot(Spot s){
        slot = s;
    }
    
    public Size getSize(){
        return size;
    }
    
    public void setSize(String s){
        if (s.compareTo("Car") == 0){
            size = size.Compact;
        }
        else {
            size = size.Moto;
        }
    }
    
    public void setRow(Row r){
        row = r;
        System.out.println("row has been set for " + this);
    }
    
    public void setCode(int c){code = c;}
    
    public int getCode(){return code;}
    
    /**
    * The code of a parked vehicle is returned with the format:
    * V001 - V999.
    */
    public String toString(){
        String sCode = "V";
        
        if (code < 10) {sCode += "00";}
        else if (code < 100) {sCode += "0";}
        
        return sCode + code;
    }
    
}
