package carpark;
import java.util.Hashtable;

/**
* This class represents a parking and it is responsible for its API i.e. to accept, allocate and refuse requests to park.
* External classes should interact with the parking through the services of this class.
* The parking offers spots for vehicles of multiple sizes.
* Available spots size are:
* small (exclusively for motorbykes);
* compact (suitable for cars and motorbykes);
* large (suitable for cars and motorbykes).
* Buses are also accepted and assigned to a row of five consequtive large spots.
*/
public class ParkingFloor{

    //@Todo: these must be private and will require a getter
    public Row[] smallSpots;
    public Row[] compactSpots;
    public RowOfLarge[] largeSpots;
    private Hashtable<Integer, Vehicle> vehicles;
    private int nCars;
    
    public static final int nSmallRows = 1;
    public static final int nCompactRows = 1;
    public static final int nLargeRows = 1;
    public static final int rowLenght = 1;
    
    /**
    * Create a new Parking floor with default parameters.
    */
    public ParkingFloor(){
        smallSpots = new Row[nSmallRows];
        compactSpots = new Row[nCompactRows];
        largeSpots = new RowOfLarge[nLargeRows];
        vehicles = new Hashtable<Integer, Vehicle>();
        nCars = 0;
        
        for (int i = 0; i < smallSpots.length; i++){
            smallSpots[i] = new Row(rowLenght * 2, Size.Moto);
        }
        
        for (int i = 0; i < compactSpots.length; i++){
            compactSpots[i] = new Row(rowLenght, Size.Compact);
        }
        
        for (int i = 0; i < largeSpots.length; i++){
            largeSpots[i] = new RowOfLarge(rowLenght * 6, Size.Large);
        }
    }
    
    /**
    * Park a vehicle.
    @param v the vehicle to park.
    @return the spot assigned to the vehicle or null in case it was impossible to park the vehicle.
    */
    public Spot park(Vehicle v){
        Size vSize = v.getSize();
        Spot foundSpot;
        vehicles.put(nCars, v);
        v.setCode(nCars);
        nCars++;
        
        if (vSize == Size.Moto) {
            foundSpot = tryToParkInRows(smallSpots, v);
            if (foundSpot != null) {
                
                return foundSpot;
            }
        }
        
        if (vSize == Size.Moto || vSize == Size.Compact) {
            foundSpot = tryToParkInRows(compactSpots, v);
            if (foundSpot != null) {
                return foundSpot;
            }
        }
        
        if (vSize == Size.Moto || vSize == Size.Compact || vSize == Size.Large) {
            foundSpot = tryToParkInRows(largeSpots, v);
            if (foundSpot != null) {
                return foundSpot;
            }
        }
        
        if (vSize == Size.Bus) {
            foundSpot = tryToParkBus(largeSpots, v);
            if (foundSpot != null) {
                return foundSpot;
            }
        }
        
        vehicles.remove(--nCars);
        return null;
    }
    
    private Spot tryToParkInRows(Row[] rows, Vehicle v){
        Spot foundSpot;
        
        for (Row r : rows){
            foundSpot = r.park(v);
            if (foundSpot != null) {
               return foundSpot;
            }
        }
        return null;
        
    }
    
    private Spot tryToParkBus(Row[] rows, Vehicle v){
        Spot foundSpot;
        
        for (Row r : rows){
            foundSpot = r.park(v);
            if (foundSpot != null) {
               return foundSpot;
            }
        }
        return null;
    }
    
    /**
    * Frees a spot previously occupied by a vehicle.
    @param code the code of a vehicle.
    @return true if a vehicle with code @code has been found in the parking, false otherwise.
    */
    public boolean leave(int code){
        Vehicle v = vehicles.remove(code);
        
        if (v == null) {return false;}
        
        v.leave();
        
        return true;
    }
}





