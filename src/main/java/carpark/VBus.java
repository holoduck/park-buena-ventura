package carpark;

/**
* Special class to handle the parking of Bus size vehicles.
*/
public class VBus extends Vehicle {
    private Spot[] slot;
    int nextSlot;
    private RowOfLarge row;
    
    protected VBus() {
        super(Size.Bus);
        slot = new Spot[5];
        nextSlot = 0;
    }
    
    public void assignSlot(Spot s){
        slot[nextSlot++] = s;
    }
    
    /**
    * Deallocate all five occupied spots.
    */
    public void leave(){
        for (int i = 0; i < 5; i++){
            slot[i].deallocate();
            slot[i] = null;
        }
        
        row.freeSlot(5);
        nextSlot = 0;
        
    }
    
    public void setRow(RowOfLarge r){
        row = r;
        System.out.println("row has been set for " + this);
    }
}
