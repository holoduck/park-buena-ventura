package carpark;

/**
* Code of a parked Vehicle Object.
*/
public class Code{
    private int code;
    
    public int getCode(){
        return code;
    }
    
    public void setCode(int c){
        code = c;
    }
    
}
